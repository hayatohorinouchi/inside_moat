json.array!(@beams) do |beam|
  json.extract! beam, :id
  json.url beam_url(beam, format: :json)
end
